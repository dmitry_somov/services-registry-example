const ServicesRegistry = require('./ServicesRegistry');

class AService {
  hello() {
    console.log('Hello A');
  }

  helloFromB() {
    const BService = ServicesRegistry.get('BService');

    new BService().hello();
  }
}

module.exports = AService;

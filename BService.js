const ServicesRegistry = require('./ServicesRegistry');

class BService {
  hello() {
    console.log('Hello B');
  }

  helloFromA() {
    const AService = ServicesRegistry.get('AService');

    new AService().hello();
  }
}

module.exports = BService;

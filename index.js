require('./ServicesRegistry/init');

const AService = require('./AService');

new AService().helloFromB();

const BService = require('./BService');

new BService().helloFromA();
const services = {};

module.exports = {
  add(Service) {
    services[Service.name] = Service;
  },

  get(serviceName) {
    return services[serviceName];
  }
};
